﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Sign
{
    class Program
    {
        static RSACryptoServiceProvider PrivateKey(X509Certificate2 publicCert)
        {
            RSACryptoServiceProvider key = null;

            X509Certificate2 privateCert = null;
            X509Store store = new X509Store(StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            foreach (X509Certificate2 cert in store.Certificates)
            {
                if (cert.GetCertHashString() == publicCert.GetCertHashString())
                    privateCert = cert;
            }

            key = new RSACryptoServiceProvider();

            key.FromXmlString(privateCert.PrivateKey
                                .ToXmlString(true));
            
            return key;
        }

        static RSACryptoServiceProvider PublicKey(X509Certificate2 publicCert)
        {
            return (RSACryptoServiceProvider)publicCert.PublicKey.Key;
        }

        static void Main(string[] args)
        {
            string data = "Test text for signing";
            byte[] dbytes = Encoding.UTF8.GetBytes(data);

            X509Certificate2 publicCert = 
                new X509Certificate2(@"C:\_dev\crypt\rsa\certs\public.cer");

            RSACryptoServiceProvider privateKey = PrivateKey(publicCert);
            byte[] sig = privateKey.SignData(dbytes, CryptoConfig.MapNameToOID("SHA256"));

            RSACryptoServiceProvider publicKey = PublicKey(publicCert);

            if (!publicKey.VerifyData(dbytes, 
                    CryptoConfig.MapNameToOID("SHA256"), sig))
                Console.WriteLine("Verified");
            else
                Console.WriteLine("Not verified");

            Console.ReadKey();
        }
    }
}



/*
 X509Certificate2 privateCert = new X509Certificate2("certificate.pfx", password, X509KeyStorageFlags.Exportable);

 // This instance can not sign and verify with SHA256:
 RSACryptoServiceProvider privateKey = (RSACryptoServiceProvider)privateCert.PrivateKey;

 // This one can:
 RSACryptoServiceProvider privateKey1 = new RSACryptoServiceProvider();
 privateKey1.ImportParameters(privateKey.ExportParameters(true));

 byte[] data = Encoding.UTF8.GetBytes("Data to be signed"); 

 byte[] signature = privateKey1.SignData(data, "SHA256");

 bool isValid = privateKey1.VerifyData(data, "SHA256", signature); 
*/
