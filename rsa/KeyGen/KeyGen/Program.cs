﻿using System.Configuration;
using System.IO;
using System.Security.Cryptography;

namespace KeyGen
{
    class Program
    {
        private static readonly string _privateKeyPath =
            ConfigurationManager.AppSettings["privateKeyPath"];

        private static readonly string _publicKeyPath =
            ConfigurationManager.AppSettings["publicKeyPath"];

        static void Main(string[] args)
        {
            using (RSACryptoServiceProvider rsa =
                new RSACryptoServiceProvider())
            {
                string privateKey = rsa.ToXmlString(true);
                string publicKey = rsa.ToXmlString(false);
                
                File.WriteAllText(_privateKeyPath, privateKey);
                File.WriteAllText(_publicKeyPath, publicKey);
            }
        }
    }
}
