﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Test
{
	public static class Encrypting
	{
		public static  byte[] Encrypt(CngKey key, byte[] data)
		{
			byte[] encrypted = null;

			using (RSACng rsa = new RSACng(key))
			{
				encrypted =
					rsa.Encrypt(data, RSAEncryptionPadding.OaepSHA256);
			}

			return encrypted;
		}

		public static byte[] Encrypt(CngKey key, string data, Encoding enc)
		{
			byte[] dbytes = enc.GetBytes(data);

			return Encrypt(key, dbytes);
		}

		public static string EncryptB64(CngKey key, byte[] data)
		{
			byte[] bytes = Encrypt(key, data);

			return Convert.ToBase64String(bytes);
		}
		
		public static string EncryptB64(CngKey key, string data, Encoding enc)
		{
			byte[] bytes = Encrypt(key, enc.GetBytes(data));

			return Convert.ToBase64String(bytes);
		}

		public static byte[] Decrypt(CngKey key, byte[] data)
		{
			byte[] decrypted = null;

			using (RSACng rsa = new RSACng(key))
			{
				decrypted =
					rsa.Decrypt(data, RSAEncryptionPadding.OaepSHA256);
			}

			return decrypted;
		}

		public static string DecryptToString(CngKey key, byte[] data, Encoding enc)
		{
			return enc.GetString(Decrypt(key, data));
		}

		public static byte[] DecryptB64(CngKey key, string data)
		{
			byte[] dbytes = Convert.FromBase64String(data);

			return Decrypt(key, dbytes);
		}

		public static string DecryptB64ToString(CngKey key, string data, Encoding enc)
		{
			byte[] dbytes = Convert.FromBase64String(data);

			return DecryptToString(key, dbytes, enc);
		}
	}
}
