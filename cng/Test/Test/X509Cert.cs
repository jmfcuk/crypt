﻿using System.Security;
using System.Security.Cryptography.X509Certificates;

namespace Test
{
	public class X509Cert
	{
		private X509Certificate2 _cert;

		public void Load(StoreLocation storeLoc, string subject)
		{
			X509Store store = new X509Store(storeLoc);
			store.Open(OpenFlags.ReadOnly);

			foreach (X509Certificate2 cert in store.Certificates)
			{
				if (cert.Subject.Contains(subject))
					_cert = new X509Certificate2(cert);
			}
		}

		public void Load(string path)
		{
			_cert = new X509Certificate2();
			_cert.Import(path);
		}

		public void Load(string path, 
						 SecureString pwd = null, 
						 X509KeyStorageFlags flags = 
						 X509KeyStorageFlags.DefaultKeySet)
		{
			_cert = new X509Certificate2();
			_cert.Import(path, pwd, flags);
		}

		public void Load(byte[] bytes)
		{
			_cert = new X509Certificate2();
			_cert.Import(bytes);
		}

		public void Load(byte[] bytes, 
						 SecureString pwd = null, 
						 X509KeyStorageFlags flags = 
						 X509KeyStorageFlags.DefaultKeySet)
		{
			_cert = new X509Certificate2();
			_cert.Import(bytes, pwd, flags);
		}
	}
}

