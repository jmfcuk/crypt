﻿using System;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Test
{
    public static class RsaKey
    {
        private const int _DEF_KEY_SIZE = 4096;

        private static CngKeyCreationParameters GetParams(int size = _DEF_KEY_SIZE)
        {
            CngKeyCreationParameters cp = new CngKeyCreationParameters();
            cp.ExportPolicy = CngExportPolicies.AllowPlaintextExport;
            cp.KeyCreationOptions = CngKeyCreationOptions.None;
            cp.KeyUsage = CngKeyUsages.AllUsages;
            cp.UIPolicy = new CngUIPolicy(CngUIProtectionLevels.None);

            cp.Parameters.Add(
                new CngProperty("Length",
                    BitConverter.GetBytes(size),
                    CngPropertyOptions.None));

            return cp;
        }
        
        public static CngKey New(string name = null, int size = _DEF_KEY_SIZE)
        {
            return CngKey.Create(CngAlgorithm.Rsa, name, GetParams(size));
        }

        public static CngKey Load(byte[] key, 
								  bool isPrivate = false)
        {
            CngKeyBlobFormat cbf =
                isPrivate == true ?
                CngKeyBlobFormat.GenericPrivateBlob :
                CngKeyBlobFormat.GenericPublicBlob;

            return CngKey.Import(key, cbf);
        }
        
        public static CngKey Load(string path, 
								  bool isPrivate = false)
        {
            byte[] bytes = File.ReadAllBytes(path);

            return Load(bytes, isPrivate);
        }

		public static CngKey Load(X509Cert cert, 
								  bool isPrivate = false,
								  SecureString pwd = null)
		{
			return null;
		}

        public static CngKey LoadB64(string b64Key, 
									 bool isPrivate = false)
        {
            byte[] bytes = Convert.FromBase64String(b64Key);

            CngKeyBlobFormat cbf =
                isPrivate == true ?
                CngKeyBlobFormat.GenericPrivateBlob :
                CngKeyBlobFormat.GenericPublicBlob;

            return Load(bytes, isPrivate);
        }

        public static CngKey LoadB64File(string path, 
										 bool isPrivate = false)
        {
            string s = File.ReadAllText(path);

            byte[] bytes = Convert.FromBase64String(s);

            return Load(bytes, isPrivate);
        }
    }
}
