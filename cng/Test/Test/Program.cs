﻿using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Test
{
    class Program
    {
        static string _path =
            ConfigurationManager.AppSettings["keyPath"];

        static void Main(string[] args)
        {
			TestEncrypting();

			TestSigning();
		}

		private static void TestEncrypting()
		{
			byte[] data = new byte[] { 0, 1, 2, 3 };

			CngKey key = RsaKey.New();

			byte[] enc = Encrypting.Encrypt(key, data);

			byte[] dec = Encrypting.Decrypt(key, enc);
		}

		private static void TestSigning()
		{
			byte[] data = new byte[] { 0, 1, 2, 3 };

			CngKey key = RsaKey.New();

			byte[] sig = Signing.Sign(key, data);

            data[3] = 9;

			bool b = Signing.Verify(key, data, sig);
		}

		private static void CreateKey()
		{
			CngKey key = RsaKey.New();

			byte[] privateKey = key.Export(CngKeyBlobFormat.GenericPrivateBlob);
			byte[] publicKey = key.Export(CngKeyBlobFormat.GenericPublicBlob);
			byte[] pkcs8 = key.Export(CngKeyBlobFormat.Pkcs8PrivateBlob);

			File.WriteAllBytes(_path + "private.key", privateKey);
			File.WriteAllBytes(_path + "public.key", publicKey);
			File.WriteAllBytes(_path + "pkcs8.key", pkcs8);

			File.WriteAllText(_path + "private.txt", Encoding.UTF8.GetString(privateKey));
			File.WriteAllText(_path + "public.txt", Encoding.UTF8.GetString(privateKey));
			File.WriteAllText(_path + "pkcs8.txt", Encoding.UTF8.GetString(privateKey));

			File.WriteAllText(_path + "private.b64", Convert.ToBase64String(privateKey));
			File.WriteAllText(_path + "public.b64", Convert.ToBase64String(publicKey));
			File.WriteAllText(_path + "pkcs8.b64", Convert.ToBase64String(pkcs8));
		}
	}
}
