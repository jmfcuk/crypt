﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Test
{
    public static class Signing
    {
        public static byte[] Sign(CngKey key, byte[] data)
        {
            byte[] signed = null;

            using (RSACng rsa = new RSACng(key))
            {
                signed =
                    rsa.SignData(data,
                                 HashAlgorithmName.SHA256,
                                 RSASignaturePadding.Pkcs1);
            }

            return signed;
        }

        public static byte[] Sign(CngKey key, string data, Encoding enc)
        {
            return Sign(key, enc.GetBytes(data));
        }

        public static string SignB64(CngKey key, byte[] data)
        {
            return Convert.ToBase64String(Sign(key, data));
        }

        public static string SignB64(CngKey key, string data, Encoding enc)
        {
            return Convert.ToBase64String(Sign(key, data, enc));
        }

        public static bool Verify(CngKey key, byte[] data, byte[] sig)
        {
            bool b = false;

            using (RSACng rsa = new RSACng(key))
            {
                b =
                    rsa.VerifyData(data,
                                   sig,
                                   HashAlgorithmName.SHA256,
                                   RSASignaturePadding.Pkcs1);
            }

            return b;
        }

        public static bool Verify(CngKey key, byte[] data, string b64Sig)
        {
            byte[] sbytes = Convert.FromBase64String(b64Sig);

            return Verify(key, data, sbytes);
        }

        public static bool Verify(CngKey key, string data, Encoding enc, byte[] sig)
        {
            byte[] dbytes = enc.GetBytes(data);

            return Verify(key, dbytes, sig);
        }

        public static bool Verify(CngKey key, string data, Encoding enc, string b64Sig)
        {
            byte[] dbytes = enc.GetBytes(data);

            return Verify(key, dbytes, Convert.FromBase64String(b64Sig));
        }
    }
}
