# pair
openssl req -x509 -newkey rsa:4096 -sha256 -nodes -subj /CN=localhost -keyout key.pem -out cert.pem -days 1825

---------------------------------------------------------------------------------------

# private key
openssl genrsa -out private.key 4096

# public key (x509 cert format)
openssl req -new -x509 -sha256 -subj "/OU=test-ou /CN=john-test" -key private.key -out public.cer -days 1825

# combine
openssl pkcs12 -export -out public-private-pkcs12.pfx -inkey private.key -in public.cer

---------------------------------------------------------------------------------------

# private/public pair
openssl req -x509 -sha256 -newkey rsa:4096 -subj "/OU=test-ou /CN=john-test" -keyout private.key -out public.cert -days 1825

# combine
openssl pkcs12 -export -inkey private.key -in public.cer -out certificate.pfx

---------------------------------------------------------------------------------------

